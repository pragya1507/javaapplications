package com.example.learning.springApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringAppApplication {
	
	public static void main(String[] args) {
		ApplicationContext appContext = SpringApplication.run(SpringAppApplication.class, args);
		BinarySearchImpl binarySearch = appContext.getBean(BinarySearchImpl.class);
		int result = binarySearch.binarySearch(new int[] {1, 3, 6, 12}, 3);
		System.out.println(result);
	}

}
