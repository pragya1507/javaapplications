package com.example.learning.springApp;

public interface SortAlgorithm {
	public int[] sort(int[] numbers);
}
